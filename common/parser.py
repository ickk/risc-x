from .exceptions import *
from .decorators import *
from .expression import *
from .parser_base import *
from .types import *

class Parser(ParserBase):
	DEBUG = False

	def _read_expression(self):
		try:
			self._proceed_through_newlines()
			expression = Expression(self.line)
			while ((old_position:=self.position) or True):
				self._proceed_through_whitespace()
				if word := self._read_word():
					expression.match(word)
				elif comment := self._read_comment():
					expression.comment = comment
					break
				elif self.next_char.is_expression_end:
					self.next_char.read()
					break
				elif self.next_char.is_newline or self.next_char.is_eof:
					break
				if self.position == old_position:
					raise ParserStuckError(f"Stuck reading expression on line: {self.line}, at position: {self.position_on_line+1}, char: `{self.next_char.peek}`")
			return expression.final()
		except ParserSyntaxError as e:
			raise ParserSyntaxError(f"line: {self.line}\n  {str(e)}")

	def _resolve_symbols(self, expression):
		for i, arg in enumerate(expression.args):
			if isinstance(arg, Symbol):
				expression.args[i] = (d := Decimal.match(str(self.expr_table.get_index(arg))))
				if expression.operator.arg_widths[i] < len(d.bin_string):
					raise ResolveError(f"line: {expression.line}\n  Resolved {arg} to 0x{d.value:X}, but {expression.operator} supports a max of 0x{2**expression.operator.arg_widths[i]-1:X}")

	@staticmethod
	def _emit_instruction(expression):
		for arg in expression.args:
			if not isinstance(arg, RawType):
				raise AssertionError(f"line: {expression.line}\n  Can't emit instructions for non-RawType {arg}")
		return f"{expression.operator.bin_string}{''.join(arg.bin_string.zfill(arg_width)[-arg_width:] for arg, arg_width in zip(expression.args, expression.operator.arg_widths))}"

	@pretty_error(e=(ParserSyntaxError, DuplicateKeyError, ResolveError, AssertionError))
	def parse(self):
		while not self.next_char.is_eof and (old_position := self.position or True):
			if (expr:= self._read_expression()):
				self.expr_table.append(expr)
			if self.position == old_position:
				raise ParserStuckError(f"Stuck reading expression on line: {self.line}, at position: {self.position_on_line+1}, char: `{self.next_char.peek}`")
		for expr in self.expr_table:
			expr.resolve(self.expr_table)
			print(f"{expr}") if self.DEBUG else None
			print(f"{f'{expr.index:X}'.zfill(1+(len(self.expr_table)-1)//16)}: {self._emit_instruction(expr)}") if self.DEBUG else print(self._emit_instruction(expr))
