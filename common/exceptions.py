
class Error(Exception):
	pass
class DuplicateKeyError(Error):
	pass
class ParserStuckError(Error):
	pass
class ParserSyntaxError(Error):
	pass
class ResolveError(Error):
	pass
