import sys
from functools import wraps, partial

def allow_exceptions(func=None, *, e=None, ret=None):
	if func is None:
		return partial(allow_exceptions, e=e, ret=ret)
	if e is None:
		raise TypeError
	if not isinstance(e, tuple):
		e = (e,)
	@wraps(func)
	def wrapper(*args, **kwargs):
		try:
			return func(*args, **kwargs)
		except e:
			return ret
	return wrapper

def pretty_error(func=None, *, e=None):
	if func is None:
		return partial(pretty_error, e=e)
	if e is None:
		e = Exception
	if isinstance(e, tuple):
		es = e
	else:
		es = (e,)
	@wraps(func)
	def wrapper(*args, **kwargs):
		try:
			return func(*args, **kwargs)
		except es as error:
			print(f"{error.__class__.__name__}: {error}")
			for i, e in enumerate(es,1):
				if error.__class__ is e:
					sys.exit(i)
	return wrapper

def catch_and_throw(func=None, *, c=None, t=None):
	if func is None:
		return partial(catch_and_throw, c=c, t=t)
	if c is None or t is None:
		raise TypeError
	if not isinstance(c, tuple):
		c = (c,)
	@wraps(func)
	def wrapper(*args, **kwargs):
		try:
			return func(*args, **kwargs)
		except c as e:
			raise t(str(e))
	return wrapper
