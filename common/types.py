import re
from collections.abc import Iterable
from inspect import isclass

class RawType():
	pass

class Opcode():
	def __init__(self, name, bin_string, args=None, **options):
		self._pattern = re.compile(r"^"+name+r"$")
		self._name = name
		self._bin_string = bin_string
		self._get_args(args)
		self.options = options
	def _get_args(self, args):
		self._arg_widths, args = zip(*args) if args else ((),())
		self._args = []
		for t in args:
			if isinstance(t, Iterable):
				self._args.append(set())
				for subt in t:
					if isinstance(subt, Iterable):
						self._args[-1].update(subt)
					else:
						self._args[-1].add(subt)
			else:
				self._args.append(set((t,)))
		self._args = tuple(frozenset(ts) for ts in self._args)
	def __len__(self):
		return 1+len(self._args)
	def __repr__(self):
		return f"<opcode {self.name}>"
	@property
	def name(self):
		return self._name
	@property
	def bin_string(self):
		return self._bin_string
	@property
	def args(self):
		return self._args
	def match(self, raw_string):
		if self._pattern.fullmatch(raw_string):
			return self
		return False
	@property
	def arg_widths(self):
		return self._arg_widths

class RawOpcode(Opcode, RawType):
	def __init__(self, name, bin_string, args=None, **options):
		super().__init__(name, bin_string, args, **options)
		for type_set in self.args:
			for t in type_set:
				if not (isinstance(t, RawType) or (isclass(t) and issubclass(t, RawType))):
					raise TypeError(f"{self.__class__.__name__} `{self.name}` can only accept args that are a subtype of {RawType.__name__}, but given {t}")
	def __repr__(self):
		return f"<raw-opcode {self.name}>"
	@property
	def arg_widths(self):
		return self._arg_widths

class Register(RawType):
	def __init__(self, name, bin_string):
		self._name = name
		self._bin_string = bin_string
		self._pattern = re.compile(r"^"+name+r"$")
	def __repr__(self):
		return f"<register {self.name}>"
	@property
	def name(self):
		return self._name
	@property
	def bin_string(self):
		return self._bin_string
	def match(self, raw_string):
		if self._pattern.fullmatch(raw_string):
			return self
		return False

class _Literal():
	def __init__(self, literal):
		if self.__class__ is _Literal:
			if isinstance(literal, str):
				self._raw_string = literal
			else:
				raise TypeError(f"{_Literal} constructor expects literal of type {str}")
		elif isinstance(literal, _Literal):
			self._raw_string = literal.raw_string
		else:
			raise TypeError(f"{self.__class__} constructor expects literal of type {_Literal}")
	@property
	def _repr_str(self):
		return f"{self.__class__.__name__.lower()} {self.raw_string}"
	def __repr__(self):
		return f"<{self._repr_str}>"
	def __eq__(self, other):
		return isinstance(other, self.__class__) and (self.raw_string == other.raw_string)
	@classmethod
	def match(cls, raw_string):
		if cls.PATTERN.fullmatch(raw_string):
			return cls(_Literal(raw_string))
		return False
	@property
	def raw_string(self):
		return self._raw_string
	# Subclasses to implement
	PATTERN = None

class StringLiteral(_Literal):
	def __init__(self, literal):
		super().__init__(literal)
		self._string = None
	def __eq__(self, other):
		return isinstance(other, StringLiteral) and (self.string == other.string)
	def __hash__(self):
		return hash(self.string)
	@property
	def string(self):
		if self._string is None:
			self._string = self.to_string(self.raw_string)
		return self._string
	# Subclasses to implement
	PATTERN = None
	@staticmethod
	def to_string(raw_string):
		raise NotImplementedError

class Symbol(StringLiteral):
	PATTERN = re.compile(r"^:[a-zA-Z_][0-9a-zA-Z_-]*[!?]?$")
	@staticmethod
	def to_string(raw_string):
		return raw_string[1:]

class Label(StringLiteral):
	PATTERN = re.compile(r"^[a-zA-Z_][0-9a-zA-Z_-]*[!?]?:$")
	@staticmethod
	def to_string(raw_string):
		return raw_string[:-1]

class Integer(_Literal, RawType):
	def __init__(self, literal):
		super().__init__(literal)
		self._value = None
		self._bin_string = None
	def __eq__(self, other):
		return isinstance(other, Integer) and (self.value == other.value)
	@property
	def value(self):
		if self._value is None:
			self._value = self.to_int(self.raw_string)
		return self._value
	@property
	def bin_string(self):
		if self._bin_string is None:
			self._bin_string = f"{self.value:b}"
		return self._bin_string
	# Subclasses to implement
	PATTERN = None
	@staticmethod
	def to_int(raw_string):
		raise NotImplementedError

class Hexadecimal(Integer):
	PATTERN = re.compile(r"^0x[0-9a-fA-F]+$")
	@staticmethod
	def to_int(raw_string):
		return int(raw_string[2:],16)

class Binary(Integer):
	PATTERN = re.compile(r"^0b[01]+$")
	@staticmethod
	def to_int(raw_string):
		return int(raw_string[2:],2)

class Decimal(Integer):
	PATTERN = re.compile(r"^[0-9]+$")
	@staticmethod
	def to_int(raw_string):
		return int(raw_string)

class ResolvedSymbol(Integer):
	def __init__(self, symbol, val, raw_val):
		self._raw_val = raw_val
		self._value = val
		self._bin_string = None
		self._raw_string = symbol.raw_string
		self._symbol = symbol
	def __repr__(self):
		return f"<{self.symbol._repr_str} -> 0x{self._raw_val:X}>"
	@property
	def symbol(self):
		return self._symbol

INTEGERS = frozenset((
	Hexadecimal,
	Binary,
	Decimal,
))

SYMBOLS = frozenset((
	Symbol,
))
