from inspect import isclass
from .types import *
from .opcodes import *
from .registers import *
from .exceptions import *

class Expression():
	def __init__(self, line):
		self._line = line
		self._label = None
		self._operator = None
		self._args = []
		self.comment = None
		self.index = None
	def __len__(self):
		return int(bool(self.operator)) + len(self._args)
	def __bool__(self):
		return bool(self.label or len(self))
	def __repr__(self):
		return f"<expression {self.line}: {str(self.label)+' ' if self.label else ''}{self.operator} {self.args}>"
	def _match_operator(self, word):
		for opcode in OPCODES:
			if opcode := opcode.match(word):
				self._operator = opcode
				break
	def _match_argument(self, word):
		for t in self.operator.args[len(self.args)]:
			if t is Register:
				for register in REGISTERS:
					if register := register.match(word):
						self._args.append(register)
						return
			elif arg := t.match(word):
				self._args.append(arg)
				return
	def match(self, word):
		if self.label is None and len(self) == 0:
			if label := Label.match(word):
				self._label = label
				return
		if self.operator is None:
			self._match_operator(word)
			if self.operator:
				return
			raise ParserSyntaxError(f"Expected an Opcode in the first position, but found `{word}`")
		if len(self.operator.args) <= (length := len(self._args)):
			raise ParserSyntaxError(f"`{self.operator.name}` operator expects only {len(self.operator.args)} arguments, but more were provided")
		self._match_argument(word)
		if length == len(self._args):
			raise ParserSyntaxError(f"`{self.operator.name}` operator expects argument of type {', '.join(set(t.__name__ if isclass(t) else t.__class__.__name__ for t in self.operator.args[len(self.args)]))} but found `{word}`")
	def final(self, word=None):
		if word:
			self.match(word)
		if (l := len(self)) and l < len(self.operator):
			raise ParserSyntaxError(f"`{self.operator.name}` operator expects {len(self.operator.args)} arguments, but only {len(self.args)} were provided")
		return self

	def resolve(self, table):
		for i, arg in enumerate(self.args):
			if isinstance(arg, Symbol):
				mode = self.operator.options.get('symbol_resolve', 'absolute')
				rel_val = (to_index := table.get_index(arg)) - self.index
				if mode == 'relative' or mode == 'relative-signed':
					val = twos_comp(rel_val, self.operator.arg_widths[i])
				elif mode == 'relative-unsigned':
					if rel_val < 0:
						raise ResolveError(f"line: {self.line}\n  {self.operator} can only resolve symbols to unsigned values, but {Label.match(arg.string+':')} precedes {arg}")
				elif mode == 'absolute':
					val = to_index
				self.args[i] = (r := ResolvedSymbol(arg, val, rel_val))
				if self.operator.arg_widths[i] < len(r.bin_string):
					raise ResolveError(f"line: {self.line}\n  Resolved {arg} to 0x{r.value:X}, but {self.operator} supports a max of 0x{2**self.operator.arg_widths[i]-1:X}")
	@property
	def line(self):
		return self._line
	@property
	def label(self):
		return self._label
	@property
	def operator(self):
		return self._operator
	@property
	def args(self):
		return self._args

def twos_comp(val, width, negate=False):
	negate, abs_val, x = negate or val < 0, abs(val), (1 << width - 1)
	if (negate and abs_val > x) or (abs_val >= x and not negate):
		raise ResolveError(f"val={val} too large for width={width}")
	return abs_val-1 ^ x if negate else val
