from .types import Register

REGISTERS = frozenset((
	# hardwired to zero
	REG_R0 := Register(name='r0',
	                   bin_string='0000'),
	
	REG_R1 := Register(name='r1',
	                   bin_string='0001'),
	
	REG_R2 := Register(name='r2',
	                   bin_string='0010'),
	
	REG_R3 := Register(name='r3',
	                   bin_string='0011'),
	
	REG_R4 := Register(name='r4',
	                   bin_string='0100'),
	
	REG_R5 := Register(name='r5',
	                   bin_string='0101'),
	
	REG_R6 := Register(name='r6',
	                   bin_string='0110'),
	
	REG_R7 := Register(name='r7',
	                   bin_string='0111'),
	
	REG_R8 := Register(name='r8',
	                   bin_string='1000'),
	
	REG_R9 := Register(name='r9',
	                   bin_string='1001'),
	
	REG_RA := Register(name='ra',
	                   bin_string='1010'),
	
	REG_RB := Register(name='rb',
	                   bin_string='1011'),
	
	REG_RC := Register(name='rc',
	                   bin_string='1100'),
	# reserved for assembler
	REG_RR := Register(name='rr',
	                   bin_string='1101'),
	# stack pointer
	REG_SP := Register(name='sp',
	                   bin_string='1110'),
	# program counter
	REG_PC := Register(name='pc',
	                   bin_string='1111'),
))
