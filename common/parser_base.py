from .data_structures import *
from .decorators import *

MAX_WORD_LENGTH = 255

def is_newline(c):
	return c == '\n'
def is_whitespace(c):
	return c == ' ' or c == '\t'
def is_expression_end(c):
	return c == ';'
def is_comment_start(c):
	return c == '#'
def is_word_end(c):
	return is_whitespace(c) or is_newline(c) or is_expression_end(c) or is_comment_start(c)

class _NextChar():
	def __init__(self, file):
		self._file = file
		self._line = 1
		self._line_start_position = file.tell()
		self._cached_char = None
	@property
	def file(self):
		return self._file
	def read(self):
		if c := self._cached_char:
			self._cached_char = None
		elif c := self.file.read(1):
			pass
		else:
			raise EOFError
		if is_newline(c):
			self._line += 1
			self._line_start_position = self.file.tell()
		return c
	@property	
	def peek(self):
		if c := self._cached_char:
			return c
		elif c := self.file.read(1):
			self._cached_char = c
			return c
		raise EOFError	
	@property
	def is_eof(self):
		try:
			if self.peek:
				return False
		except EOFError:
			pass
		return True
	@property
	@allow_exceptions(e=EOFError)
	def is_newline(self):
		return is_newline(self.peek)
	@property
	@allow_exceptions(e=EOFError)
	def is_whitespace(self):
		return is_whitespace(self.peek)
	@property
	@allow_exceptions(e=EOFError)
	def is_comment_start(self):
		return is_comment_start(self.peek)
	@property
	@allow_exceptions(e=EOFError)
	def is_expression_end(self):
		return is_expression_end(self.peek)
	@property
	@allow_exceptions(e=EOFError)
	def is_word_end(self):
		return is_word_end(self.peek)
	@property
	def _position(self):
		# Give the position of the current char
		return self.file.tell()-int(bool(self._cached_char))

class ParserBase():
	def __init__(self, file_in, file_out=None):
		self.next_char = _NextChar(file_in)
		self.expr_table = ExpressionTable()
	def _read_word(self):
		word = []
		for _ in range(MAX_WORD_LENGTH):
			if self.next_char.is_word_end or self.next_char.is_eof:
				if len(word):
					return "".join(word)
				else:
					return None
			c = self.next_char.read()
			word.append(c)
		raise ParserStuckError(f"_read_word() reached the MAX_WORD_LENGTH of {MAX_WORD_LENGTH}" )
	def _read_comment(self):
		comment = []
		if self.next_char.is_comment_start:
			self._inside_comment = True
			comment.append(self.next_char.read())
		while self._inside_comment and not (self.next_char.is_newline or self.next_char.is_eof):
			comment.append(self.next_char.read())
		self._inside_comment = False
		return "".join(comment) or None
	def _proceed_through_whitespace(self):
		while self.next_char.is_whitespace:
			self.next_char.read()
	def _proceed_through_newlines(self):
		while self.next_char.is_newline:
			self.next_char.read()
	@property
	def position(self):
		return self.next_char._position
	@property
	def line(self):
		return self.next_char._line
	@property
	def position_on_line(self):
		return self.next_char._line_start_position - self.next_char._position
