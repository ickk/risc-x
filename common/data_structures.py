from .exceptions import *
from .types import StringLiteral

class ExpressionTable():
	def __init__(self):
		self._key_dict = {}
		self._list = []
		self._label_stack = []
	def __getitem__(self, key):
		if isinstance(key, int):
			return self._list[key]
		elif isinstance(key, StringLiteral):
			return self._list[self._key_dict[key]]
		raise KeyError(f"key `{key}` is not a valid type")
	def __iter__(self):
		return (item for item in self._list)
	def __len__(self):
		return len(self._list)
	def append(self, expression):
		if expression.label:
			self._label_stack.append(expression.label)
		if len(expression):
			self._list.append(expression)
			expression.index = len(self._list)
			for _ in range(len(self._label_stack)):
				if (label := self._label_stack.pop()) in self._key_dict:
					raise DuplicateKeyError(f"{label} was defined more than once")
				self._key_dict[label] = len(self._list)
	def get_index(self, key):
		return self._key_dict[key]
