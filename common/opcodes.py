from .types import *
from .registers import *

OPCODES = frozenset((
	OP_ADDI  := RawOpcode(name='addi',
	                      bin_string='0000',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, INTEGERS))),
	OP_SUBI  := RawOpcode(name='subi',
	                      bin_string='0001',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, INTEGERS))),
	OP_ADD   := RawOpcode(name='add',
	                      bin_string='0010',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, REGISTERS))),
	OP_SUB   := RawOpcode(name='sub',
	                      bin_string='0011',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, REGISTERS))),
	OP_AND   := RawOpcode(name='and',
	                      bin_string='0100',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, REGISTERS))),
	OP_OR    := RawOpcode(name='or' ,
	                      bin_string='0101',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, REGISTERS))),
	OP_XOR   := RawOpcode(name='xor',
	                      bin_string='0110',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, REGISTERS))),
	OP_MOD   := RawOpcode(name='mod',
	                      bin_string='0111',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, REGISTERS))),
	OP_DIV   := RawOpcode(name='div',
	                      bin_string='1000',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, REGISTERS))),
	OP_MUL   := RawOpcode(name='mul',
	                      bin_string='1001',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, REGISTERS))),
	OP_LUI   := RawOpcode(name='lui',
	                      bin_string='1010',
	                      args=((4, REGISTERS),
	                            (8, INTEGERS))),
	OP_LLI   := RawOpcode(name='lli',
	                      bin_string='1011',
	                      args=((4, REGISTERS),
	                            (8, INTEGERS))),
	OP_BRGT  :=    Opcode(name='brgt',
	                      bin_string='1100',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, {SYMBOLS, INTEGERS, REGISTERS})),
	                      symbol_resolve='relative'),
	OP_BREQ  :=    Opcode(name='breq',
	                      bin_string='1101',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS),
	                            (4, {SYMBOLS, INTEGERS, REGISTERS})),
	                      symbol_resolve='relative'),
	OP_LOAD  :=    Opcode(name='load',
	                      bin_string='11111111',
	                      args=((4, REGISTERS),
	                            (4, {SYMBOLS, REGISTERS}))),
	OP_STORE :=    Opcode(name='store',
	                      bin_string='11111110',
	                      args=((4, REGISTERS),
	                            (4, {SYMBOLS, REGISTERS}))),
	OP_SLL   := RawOpcode(name='sll',
	                      bin_string='11111101',
	                      args=((4, REGISTERS),
	                            (4, INTEGERS))),
	OP_SLR   := RawOpcode(name='slr',
	                      bin_string='11111100',
	                      args=((4, REGISTERS),
	                            (4, INTEGERS))),
	OP_NOT   := RawOpcode(name='not',
	                      bin_string='11110001',
	                      args=((4, REGISTERS),
	                            (4, REGISTERS))),
))
