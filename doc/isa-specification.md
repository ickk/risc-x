Instruction Set Architecture
============================

### Opcodes ###
| name    | type      | arguments   | instruction       | description
|:-------:|:---------:|:-----------:|:-----------------:|:----------------------------------------------------------------
| `addi`  | immediate | `d` `x` `i` | `0x0` `d` `x` `i` | add immediate `i` to value in register `x` and store result in register `d`
| `subi`  | immediate | `d` `x` `i` | `0x1` `d` `x` `i`
| `add`   | normal    | `d` `x` `y`
| `sub`   | normal    | `d` `x` `y`
| `and`   | normal    | `d` `x` `y`
| `or`    | normal    | `d` `x` `y`
| `xor`   | normal    | `d` `x` `y`
| `mod`   | normal    | `d` `x` `y`
| `div`   | normal    | `d` `x` `y`
| `mul`   | normal    | `d` `x` `y`
| `load`  | immediate
| `store` | immediate
| `brgt`  | 
| `breq`  | 
| `lui`   | long immediate
| `lli`   | long immediate
| `brlt`  | psuedo
| `brneq` | psuedo
| `jump`  | psuedo
| `modi`  | psuedo
| `muli`  | psuedo
| `divi`  | psuedo
| `ssl`   | n/a
| `slr`   | n/a
| `not`   | n/a
| `jal`   | n/a

### Registers ###
| name  | address | function
|:-----:|:-------:|:-----------------------------
| `r0`  |  `0x0`  | hardwired to the value 0x0000
| `r1`  |  `0x1`  | general purpose register
| `r2`  |  `0x2`  | general purpose register
| `r3`  |  `0x3`  | general purpose register
| `r4`  |  `0x4`  | general purpose register
| `r5`  |  `0x5`  | general purpose register
| `r6`  |  `0x6`  | general purpose register
| `r7`  |  `0x7`  | general purpose register
| `r8`  |  `0x8`  | general purpose register
| `r9`  |  `0x9`  | general purpose register
| `r10` |  `0xA`  | general purpose register
| `r11` |  `0xB`  | general purpose register
| `r12` |  `0xC`  | general purpose register
| `r13` |  `0xD`  | general purpose register
| `sp`  |  `0xE`  | stack pointer
| `pc`  |  `0xF`  | program counter
